import { FormWp, ProducList } from "../organisms";

export const HomePage = ({ cards }) => (
  <>
    <ProducList cards={cards} />
    <FormWp />
  </>
);
