import { Fragment } from "react";
import { Footer, Header } from "../organisms";
import { Container } from "react-bootstrap";

export const Layout = ({ children, header, footer }) => (
  <Fragment>
    <Header {...header} />
    <main>
      <Container>{children}</Container>
    </main>
    <Footer {...footer} />
  </Fragment>
);
