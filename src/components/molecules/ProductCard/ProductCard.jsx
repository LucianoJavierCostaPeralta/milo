import { Card, Col } from "react-bootstrap";
import { formatearMoneda } from "../../../helpers";

export const ProductCard = ({ src, name, price, description }) => {
  return (
    <Col sm={12} md={6} lg={4} className="my-3">
      <Card className="shadow">
        <Card.Header>{name && <Card.Title>{name}</Card.Title>}</Card.Header>
        {src && (
          <Card.Img
            variant="top"
            src={src ?? null}
            alt={`Focaccia - ${name}`}
            title={`Focaccia - ${name}`}
          />
        )}
        <Card.Body>
          {description && (
            <Card.Text className="text-muted">{description}</Card.Text>
          )}
        </Card.Body>
        <Card.Footer>
          {price && <Card.Text>{formatearMoneda(price)}</Card.Text>}
        </Card.Footer>
      </Card>
    </Col>
  );
};
