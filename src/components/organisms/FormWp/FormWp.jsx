import { useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";

export const FormWp = () => {
  const [formulario, setFormulario] = useState({
    nombre: "",
    apellido: "",
    pedido: "",
  });

  const { nombre, apellido, pedido } = formulario;

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormulario({ ...formulario, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    enviarWhatsApp();
  };

  const enviarWhatsApp = () => {
    const mensaje = `Nombre: ${nombre} ${apellido}\nPedido: ${pedido}`;
    const numeroWhatsApp = "+54 9 11 3377-3628"; // Reemplaza con el número de WhatsApp al que deseas enviar el mensaje

    const urlWhatsApp = `https://api.whatsapp.com/send?phone=${numeroWhatsApp}&text=${encodeURIComponent(
      mensaje
    )}`;
    window.open(urlWhatsApp);
  };
  return (
    <Form className="my-4" onSubmit={handleSubmit}>
      <Row>
        <Col sm={12}>
          <h3 className="display-6">Realizar pedido</h3>
        </Col>
        <Col md={6} className="mt-3">
          <Form.Group controlId="nombre">
            <Form.Label>Nombre</Form.Label>
            <Form.Control
              type="text"
              name="nombre"
              placeholder="Ingresa tu nombre"
              value={nombre}
              onChange={handleChange}
            />
          </Form.Group>
        </Col>
        <Col md={6} className="mt-3">
          <Form.Group controlId="apellido">
            <Form.Label>Apellido</Form.Label>
            <Form.Control
              type="text"
              name="apellido"
              placeholder="Ingresa tu apellido"
              value={apellido}
              onChange={handleChange}
            />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col className="mt-3">
          <Form.Group controlId="pedido">
            <Form.Label>Pedido</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              name="pedido"
              placeholder="Ingresa tu pedido"
              value={pedido}
              onChange={handleChange}
            />
          </Form.Group>
        </Col>
      </Row>
      <Row>
        <Col>
          <Button variant="success" type="submit" className="my-3">
            Enviar a WhatsApp {/* Agrega el icono de WhatsApp */}
          </Button>
        </Col>
      </Row>
    </Form>
  );
};
