import { Container } from "react-bootstrap";

export const Header = ({ title }) => (
  <header className="py-3">
    <Container>{title && <h1 className="display-2">{title}</h1>}</Container>
  </header>
);
