import { Row } from "react-bootstrap";
import { ProductCard } from "../../molecules";

export const ProducList = ({ cards }) => (
  <>
    {cards?.length && (
      <Row>
        {cards?.map((item, index) => {
          return <ProductCard key={index} {...item} />;
        })}
      </Row>
    )}
  </>
);
