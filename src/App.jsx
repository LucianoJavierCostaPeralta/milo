import { HomePage, Layout } from "./components/template";
import useStaticData from "./hooks/useStaticData";

function App() {
  const { defaultData } = useStaticData();
  return (
    <Layout {...defaultData.layout}>
      <HomePage {...defaultData.homePage} />
    </Layout>
  );
}

export default App;
