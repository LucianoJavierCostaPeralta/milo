const useStaticData = () => {
  const defaultData = {
    layout: {
      header: {
        title: "Milo Focacceria",
      },

      footer: {
        title: "Footer",
      },
    },
    homePage: {
      cards: [
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
        {
          name: "Tomates cherry y pesto de albaca",
          src: "/assets/f-tomateyalbacawebp",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit consectetur nam ad, praesentium dignissimos rem fugit necessitatibus corporis! Inventore, non.",
          price: 1500,
        },
      ],
    },
  };

  return {
    defaultData,
  };
};

export default useStaticData;
