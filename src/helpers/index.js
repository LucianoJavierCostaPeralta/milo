export const formatearMoneda = (monto) =>
  monto.toLocaleString("es-AR", {
    style: "currency",
    currency: "ARS",
  });
